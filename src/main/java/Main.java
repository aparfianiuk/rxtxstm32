import gnu.io.*;
import java.io.*;
import java.util.Enumeration;

public class Main {

    public static void main(String[] args) throws IOException {

        System.out.println(System.getProperty("os.name").toLowerCase());
        System.out.println(System.getProperty("os.arch").toLowerCase());

        Enumeration thePorts = CommPortIdentifier.getPortIdentifiers();
        CommPortIdentifier commPortIdentifier;
        InputStream inputStream = null;
        OutputStream outputStream;
        CommPort port = null;
        byte[] buffer = new byte[1024];
        int data;

        try {
            commPortIdentifier = CommPortIdentifier.getPortIdentifier("COM3");
            port = commPortIdentifier.open("CommUtil",2000);
            SerialPort serialPort = (SerialPort) port;
            serialPort.setSerialPortParams(115200,SerialPort.DATABITS_8,SerialPort.STOPBITS_1,SerialPort.PARITY_NONE);
        } catch (PortInUseException | UnsupportedCommOperationException | NoSuchPortException e) {
            e.printStackTrace();
        }

        try {
            inputStream = port.getInputStream();
            outputStream = port.getOutputStream();
            outputStream.write('O');
        } catch (IOException e) {
            e.printStackTrace();
        }

        while (true){
        int len = 0;
        while ((data = inputStream.read())>-1){
                if ( data == 13 || len == 1024 ) {
                    break;
                }
                buffer[len++] = (byte) data;
        }
        if(len>0){
            System.out.println(new String(buffer,0,len));}
        }

    }
}
